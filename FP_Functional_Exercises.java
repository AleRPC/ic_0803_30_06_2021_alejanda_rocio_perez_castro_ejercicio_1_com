import java.util.List;

public class FP_Functional_Exercises{ 
	
	public static void main(String[] args){
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

		List<String> courses = List.of("Spring", "Spring Boot", "API", 
			"Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");

		System.out.println("Ejercicio 1");
		System.out.println("\tFORMA 1.1");
		numbers.stream()
		.filter(numeros -> numeros % 2 != 0)
		.forEach(numeros -> System.out.print(" " + numeros));

		System.out.println("\n\tFORMA 1.2");
		numbers.stream()
		.filter(FP_Functional_Exercises::numeroImpar)
		.forEach(numeros -> System.out.print(" " + numeros));

		System.out.println("\n\tFORMA 1.3");
		numbers.stream()
		.filter(FP_Functional_Exercises::numeroImpar)
		.forEach(FP_Functional_Exercises::print);

		System.out.println("\n");

		System.out.println("Ejercicio 2");
		System.out.println("\tFORMA 2.1");
		courses.stream().forEach(le -> System.out.print(le + ", "));

		System.out.println("\n\tFORMA 2.2");
		courses.stream().forEach(FP_Functional_Exercises::print);

		System.out.println("\n");

		System.out.println("Ejercicio 3");
		courses.stream()
		.filter(palabra -> palabra.contains("Spring"))
		.forEach(FP_Functional_Exercises::print);		

		System.out.println("\nEjercicio 4");
		courses.stream()
					.filter(course -> course.length() >= 4)
					.forEach(FP_Functional_Exercises::print);
		System.out.println("\n");	

		System.out.println("\nEjercicio 5");
		numbers.stream()
					.filter(number -> number % 2 != 0)
					.map(number -> number * number * number)
					.forEach(FP_Functional_Exercises::print);
		System.out.println("\n");

		System.out.println("\nEjercicio 6");
		courses.stream()
				.map(course -> course + " = " + course.length())
				.forEach(FP_Functional_Exercises::print);
		System.out.println("");		

		System.out.println("\n");
	}

	private static boolean numeroImpar(int entero){
		return (entero % 2 != 0);
	}

	private static void print(int numero){
		System.out.print(numero + ", " );
	} 

	private static void print(String palabra){
		System.out.print(palabra + ", " );
	}
}